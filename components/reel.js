AFRAME.registerComponent('reel',{
    init: function() {
      console.log("hello I'm a reel")
    },
    update: function () {
        // This will log the `message` when the entity emits the `event`.
        this.el.addEventListener('setFinalTiles', ()=> {
          this.setFinalTiles([Math.floor((Math.random() * 4 ) + 1),Math.floor((Math.random() * 4 ) + 1),Math.floor((Math.random() * 4 ) + 1)])
        });
        
        this.el.addEventListener('highlightSymbol', ()=> {
          this.highlightSymbol(1)
        });
    },
    setFinalTiles: function(symbols) {
        this.emptyReel()
        var data = this.data;  // Component property values.

        for (var i = 0; i < symbols.length; i++) {        
            var el = document.createElement('a-entity');
            let finalY = (i * 2) + 3 //+ (Math.random() * 0.1)
            el.setAttribute("position",  "0 " + finalY + " -5");

            if(symbols[i] === 1) {
                el.setAttribute("gltf-model", "#sym1");
                el.setAttribute("scale", "1 1 1");
            } else if (symbols[i] === 2) {
                el.setAttribute("gltf-model", "#sym2");
                el.setAttribute("scale",  "0.15 0.1 0.15");
            } else if (symbols[i] === 3) {
                el.setAttribute("gltf-model", "#sym3");
                el.setAttribute("scale",  "0.15 0.1 0.15");
                //el.setAttribute("geometry",  "primitive: box; width: 3; height:1; depth:1;");

                //el.setAttribute("light",  "type: point; intensity: 2.0");
            } else if (symbols[i] === 4) {
                el.setAttribute("gltf-model", "#sym4");
                el.setAttribute("scale", "1 1 1");
            }
            //el.setAttribute("geometry",  "primitive: box;");
            el.setAttribute("dynamic-body", "shape: box; mass: 100");
            this.el.appendChild(el);
        }
    },
    emptyReel: function() {
        while (this.el.firstChild) {
            this.el.removeChild(this.el.firstChild);
        }
    },
    highlightSymbol: function(symPos) {
        
        let children = this.el.getChildren()        
        //for (var i = 0; i < children.length; i++) {
            var anim = document.createElement('a-animation');
            anim.setAttribute("attribute", "scale");
            anim.setAttribute("begin", "0");
            anim.setAttribute("dur", "800");
            anim.setAttribute("direction", "alternate-reverse");

            console.log(  children[symPos].getAttribute('scale').x)
            anim.setAttribute("to", children[symPos].getAttribute('scale').x * 0.8 + ' ' +  children[symPos].getAttribute('scale').y * 0.8 + ' '  + children[symPos].getAttribute('scale').z * 0.8);
            anim.setAttribute("repeat", "10");     
            children[symPos].appendChild(anim);
            
            
        //}
        

        
        // anim.addEventListener('animationstart', () => {
        //     el.setAttribute("static-body", "true");
        // })
    }
});