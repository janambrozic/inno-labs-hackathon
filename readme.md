

Dev material:
https://hacks.mozilla.org/2018/03/immersive-aframe-low-poly/?utm_source=dev-newsletter&utm_medium=email&utm_campaign=mar8-2018


3D asset sources:
https://sketchfab.com/models/7403b639fdcb4d20b58c6f51fc20b324#
https://sketchfab.com/models/ef5756fe6a38442f9d3673857011077d#
https://sketchfab.com/models/1a85945dd2a840f594bf6cb003176a54
https://sketchfab.com/models/3db52074566341c3ba8c7b17ec181d65#
https://sketchfab.com/models/94a24190869d4cb28fc8d670252de71b#
