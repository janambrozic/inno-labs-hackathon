const $ = (sel) => document.querySelector(sel)
const $$ = (sel) => document.querySelectorAll(sel)
const on = (elem, type, hand) => elem.addEventListener(type,hand)

AFRAME.registerComponent('slotmachine', {
  init: function () {
    // var sceneEl = this.el.sceneEl
    // var el = sceneEl.querySelector('#sym')
    // console.log(el);
    // console.log(el.getAttribute('dynamic-body'));

  }
});

AFRAME.registerComponent('force-pushable', {
  schema: {
    force: { default: 10 }
  },
  init: function () {
      console.log('hey')
      this.pStart = new THREE.Vector3();
     this.el.addEventListener('click', this.forcePush.bind(this));
  },
  forcePush: function () {
    var force,
        sceneEl = this.el.sceneEl,
        pStart = sceneEl.querySelector('#sym')

    // Compute direction of force, normalize, then scale.
    // force = el.body.position.vsub(pStart);
    // force.normalize();
    // force.scale(this.data.force, force);
    // 
    // el.body.applyImpulse(force, el.body.position);
    pStart.body.sleep()
  }
});

const resetBall = () => {
    clearTimeout(resetId)
    $("#ball").body.position.set(0, 0.6,-4)
    $("#ball").body.velocity.set(0, 5,0)
    $("#ball").body.angularVelocity.set(0, 0,0)
    hit = false
    resetId = setTimeout(resetBall,6000)
}

let hit = false
let resetId = 0
let balance = 10000
on($("#weapon"),'collide',(e)=>{
    const spinButton = $("#spin-button")
    if(e.detail.body.id === spinButton.body.id) {
        hit = true
        clearTimeout(resetId)
        resetId = setTimeout(resetBall,2000)
    }
})

let firstTime = true;

$('#spin-button').addEventListener('click', function() {
    if(firstTime) {
        $("#initial-help-text").parentNode.removeChild($("#initial-help-text"));
        firstTime = false
    }
    balance -= 100
    $("#balance").setAttribute('text','value','Balance: ' + (balance))
    
    for (var i = 0; i < 5; i++) {
        $("#reel" + i).emit('setFinalTiles')
    }
    
    setTimeout(()=>{
        console.log('highlight')
        for (var i = 0; i < 5; i++) {
            $("#reel" + i).emit('highlightSymbol')
        }
    }, 2000)
    
    $('#spin-button').emit('spin');

    
    
//<a-box dynamic-body position=" 0 1.5 -3" rotation="0 0 0" color="#4C22D9"></a-box>

});

//setTimeout(resetBall,3000)

